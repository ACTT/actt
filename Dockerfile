FROM node:8

WORKDIR /usr/src/app

COPY package*.json ./
COPY config/smtp.json ./config

RUN npm install

COPY . .

EXPOSE 3001 8080

CMD node index.js


