# ACTT

De client repository (ACTT-Client) kan gevonden worden op: https://gitlab.com/ACTT/actt-client/

## Installatie via  Docker

Clone de projecten ACTT (https://gitlab.com/ACTT/actt.git) en ACTT-Client (https://gitlab.com/ACTT/actt-client.git) vanuit gitlab. 

Navigeer naar de projectfolder, controleer of hier ook een Dockerfile in staat.

Voor de e-mailconfiguratie moet er een configuratiebestand aangemaakt worden in de folder: config/

De configuratie wordt opgeslagen als smtp.json

Een voorbeeld configuratie wordt hieronder weergeven, hierbij is user het e-mailaccount wat gebruikt wordt om een e-mail te versturen en to het e-mailaccount waarop de updates worden ontvangen:

```
{
    "host":"smtp.gmail.com",
    "port":"587",
    "name":"Naam",
    "user":"Email@email.com",
    "pass":"Wachtwoord",
    "to":"Bedrijfsemail@mail.com"
}
```

In de projectfolder voer het volgende commando uit:

```
sudo docker build -t “naam” -f Dockerfile.naam .
```

De -t optie geeft een naam aan de image. 
De -f optie specificeert de Dockerfile voor de image. 
Als laatste argument wordt het pad meegegeven van het project. Wanneer dit commando in de projectfolder uitgevoerd wordt is er alleen een . nodig. 

Er wordt aangeraden om in de ACTT-Client folder het volgende commando uit te voeren:

```
sudo docker build -t frontend -f Dockerfile .
```

En in de ACTT folder de volgende commando’s:

```
sudo docker build -t backend -f Dockerfile .
sudo docker build -t backend-external -f Dockerfile.external .
```

Wanneer het commando ```sudo docker images``` wordt uitgevoerd staan er de volgende images beschikbaar:
* frontend
* backend
* backend-external


Om deze images te laten draaien in containers worden de volgende commando’s uitgevoerd:

```
sudo docker run -p “eigen_poort”:3000 -d frontend
sudo docker run -p “eigen_poort”:3001 -d backend
sudo docker run -p “eigen_poort” 3019 -d backend-external
```

Hierbij geeft de -p optie aan op welke poort de applicatie lokaal moet draaien.
De -d optie geeft aan dat de container in detached mode moet draaien. Hierdoor kunnen er meerdere commando’s uitgevoerd worden zonder dat deze in de container uitgevoerd worden.

Om te controleren welke containers er draaien wordt de volgende commando uitgevoerd:

```
sudo docker ps
```
Bij bovenstaand commando komen ook container ID’s naar voren. Deze zijn nodig om een container te stoppen.

Wanneer er bijvoorbeeld een nieuwe versie is van de ACTT is het ook noodzakelijk om hiervan een nieuwe docker image te maken. Dit proces gaat alsvolgt:

```
sudo docker ps
```
Om te controleren welke containers er zijn.

```
sudo docker kill “eerste 4 tekens van het container ID”
```

Bovenstaand commando stopt de container, hieraan hoeven alleen de eerste 4 tekens van het container ID aan meegegeven worden.

## Het verwijderen van ACTT
Onderstaande commando’s zijn alleen nodig als de image ook verwijderd moet worden.

```
sudo docker system prune
```

Bovenstaand commando ruimt de gestopte containers op, dit is nodig voordat de image verwijderd kan worden. 

```
sudo docker rmi “imagenaam of ID”
```

Bovenstaande commando verwijdert de image op imagenaam of ID.

Vervolgens moet er opnieuw een image gemaakt worden zoals eerder beschreven in de handleiding.  

Voor meer informatie over Docker zie: https://docs.docker.com/
