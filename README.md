# ACTT

ACTT is a Access Control Testing Tool that helps you guarantee the implentation of acces control within your applications. 

# Introduction

This tool was written as a school assignment for the Minor Cyber Security Engineering at Fontys University of Applied Sciences. The aim of the assignment was to create a product that solves a relevant security problem. We as security students missed a tool which is mainly aimed at verifying access controls on websites/APIs.

After research it turned out that there is no open-source tool that checks access controls. Seeing how most of us had affinity with programming we decided to make it ourselves. 

# Features

- Define login requests of the usertypes you'd like to test
- Define requests that need to be validated
- Start a scan which will validate the user permissions 
- See the results of said scan
- Set a goal by approving or disapproving scan results
- See delta's of the previous and the current scan
- See delta's of the current and your goal
- Receive an email after a scan has completed with the scan results and delta's
- Ability to automate scan's through a webhook

## Screenshots
### Homepage

![Homepage](https://gitlab.com/ACTT/actt/raw/master/images/home.png)

### Projectpage

![Project](https://gitlab.com/ACTT/actt/raw/master/images/project.png)

### Creating/editing of requests
![Requests](https://gitlab.com/ACTT/actt/raw/master/images/request.png)

#### Results of a scan
![Results](https://gitlab.com/ACTT/actt/raw/master/images/results.png)

# Installation
## Manual installation

### Front-end
In the project directory run: ```npm start``` to start the front-end. This is an React application that communicates with the Node back-end API. Projects can be created, edited and scans can be scheduled on behalf of the back-end. 

### Back-end
In the project directory run: ```node index.js``` to start the back-end. Scans that are started from the front-end will be executed by this back-end API. Requests are validated against the test server. 

### External back-end
In the project directory run: ```node externalIndex.js``` to start the external back-end server. The external back-end server has to be started with one argument as ip address. This server acts as a forward proxy to route scan calls to the internal back-end server. This makes it possible to launch scans from outside the internal network through the means of a webhook.

## Installation via Docker

Clone the projects ACTT (https://gitlab.com/ACTT/actt.git) and ACTT-Client (https://gitlab.com/ACTT/actt-client.git) from gitlab.

Navigate to the project folder, check whether a Docker file is included.

For the e-mail configuration, a configuration file must be created in the folder: /config

The configuration is saved as smtp.json

A sample configuration is shown below, ```user``` is the e-mail account used to send an e-mail and ```to``` is the e-mail account where the updates are received:

```
{
    "host": "smtp.gmail.com",
    "port": "587",
    "name": "Name",
    "user": "Email@email.com",
    "pass": "Password",
    "to": "Bedrijfsemail@mail.com"
}
```

In the project folder, run the following command:

```
sudo docker build -t "name" -f Dockerfile.name.
```

The -t option gives a name to the image.
The -f option specifies the Docker file for the image.
The final argument is the path of the project. When this command is executed in the project folder, there is only one ```.``` required.

It is recommended to run the following command in the ACTT-Client folder:

```
sudo docker build -t frontend -f Dockerfile.
```

And in the ACTT folder the following commands:

```
sudo docker build -t backend -f Dockerfile.
sudo docker build -t backend-external -f Dockerfile.external.
```

When the ```sudo docker images``` command is executed, the following images are available:
* frontend
* backend
* backend-external

To have these images run in containers, the following commands are executed:

```
sudo docker run -p "custom_port": 3000-d frontend
sudo docker run -p "custom_port": 3001 -d backend
sudo docker run -p "custom_port" 3019 -d backend-external
```

The -p option indicates at which port the application should run locally.
The -d option indicates that the container must run in detached mode. As a result, multiple commands can be executed without being executed in the container.

To check which containers are running, the following command is executed:

```
sudo docker ps
```
Container IDs will be shown when the command above is executed. These are needed to stop a container.

If, for example, there is a new version of ACTT, it is also necessary to make a new docker image. This process is as follows:

```
sudo docker ps
```
The above command checks which containers are runnning.

```
sudo docker kill "first 4 characters of the container ID"
```

The above command stops the container, only the first 4 characters of the container ID have to be given.

### Removing ACTT
The commands below are only required if the image has to be removed.

```
sudo docker system prune
```

The above command clears the stopped containers, this is necessary before the image can be removed.

```
sudo docker rmi "image name or ID"
```

The above command removes the image on image name or ID.

Then an image has to be made again as described earlier in the manual.

For more information about Docker see: https://docs.docker.com/

# Frontend
This is the back-end part of the ACTT tool. You can find the front-end on: https://gitlab.com/ACTT/actt-client.

# Future
TO-DO: tbd