var express = require("express");
var bodyParser = require("body-parser");
var request = require("unirest");

var externalIP;
if(!process.argv[2] || process.argv[2] === ""){
    console.log('Specify the IP adress of the internal server as first argument!');
    process.exit();
}

externalIP = process.argv[2];
console.log("Using internal IP adress: " + externalIP);

var app = express();
var cors = require("cors");

app.use(cors());
app.use(express.json());

app.use(
    bodyParser.json({
        limit: "50mb",
    }),
);
app.use(
    bodyParser.urlencoded({
        limit: "50mb",
        extended: true,
        parameterLimit: 50000,
    }),
);

app.get("/start/:name", function(req, res) {
    let req2 = {};
    req2.url = "http://" + externalIP + ":3001/projects/" + req.params.name + "/startscan";
    request.get(req2.url).end(function(response) { });
    res.send("Scan started for " + req.params.name);
});

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;

    // render the error page
    res.status(err.status || 500);
});

app.listen(3019);

// // projectUtils.updateProject(project)
// projectUtils.saveProject(project)

// scan.startScan(projectUtils.getProjectByName("theverybesttest"))
