// @ts-ignore
const unirest = require('unirest');

function customRequest(requestOptions, userType) {
    var request;
    if (requestOptions.url) {
        request = setMethod(requestOptions);
    } else {
        console.log("url aint okay")
        return null
    }

    request = setHeaders(request, requestOptions)
    request = setBody(request, requestOptions)


    if (requestOptions.requestType === "login") {
        return placeLoginRequest(request, requestOptions)
    } else if (requestOptions.requestType === "validate") {
        return placeTestRequest(request, requestOptions, userType)
    }
}

function setMethod(options) {
    switch (options.requestMethod) {
        case "get":
            return unirest.get(options.url);
        case "post":
            return unirest.post(options.url);
        default:
            break;
    }
}

function setHeaders(request, options) {
    if (options.headers && Object.keys(options.headers).length !== 0) {
        request.headers(options.headers)
    }
    return request
}

function setBody(request, options) {
    if (options.body && options.body !== "") request.send(options.body)
    return request
}

function placeLoginRequest(request, options) {
    return new Promise((resolve, reject) => {
        request.end(function (response) {
            switch (options.authType) {
                case "header":
                    resolve(getAuthFromHeader(response, options.authKey))
                    break;
                case "json":
                    resolve(getAuthFromJSON(response, options.authKey))
                    break;
                case "plaintext":
                    resolve(getAuthFromPlainText(response))
                    break;
                default:
                    reject("string")
                    break;
            }
        });
    })
}

function placeTestRequest(request, options, userType) {
    return new Promise((resolve, reject) => {
        request.end(function (response) {
            var isIncorrect;
            var bodyIncorrect;
            var headerIncorrect;
            var codesIncorrect;
            var headerTest = "";

            if (!response.headers) {
                reject(response)
            }
            else {
                Object.keys(response.headers).forEach(function (key) {
                    var val = response.headers[key];
                    headerTest += key + ': ' + val + '\n';
                });

                if (options.bodyRegEx) {
                    bodyIncorrect = new RegExp(options.bodyRegEx, 'gmi').test(JSON.stringify(response.body))
                }
                if (options.headerRegEx) {
                    headerIncorrect = new RegExp(options.headerRegEx, 'gmi').test(headerTest)
                }
                if (options.statusCodes) {

                    codesIncorrect = options.statusCodes.includes(response.code)
                }

                isIncorrect = bodyIncorrect || headerIncorrect || codesIncorrect
                // console.log(response.code, userType, options.url, isIncorrect);
                let result = { 'user': userType, 'allowed': !isIncorrect }//[userType, !isIncorrect]

                resolve(result)
            }

        });
    })
}

function getAuthFromHeader(response, key) { 
    key = key.toLowerCase();
    if (response.headers[key]) return response.headers[key]
    else return false
}

function getAuthFromJSON(response, key) {
    key = key.toLowerCase();
    if (response.body[key]) return response.body[key]
    else return false
}

function getAuthFromPlainText(response) {
    return response.body
}


function isURL(str) {
    var urlRegex = "@(https?|ftp)://(-\.)?([^\s/?\.#-]+\.?)+(/[^\s]*)?$@iS";
    var url = new RegExp(urlRegex, 'i');
    return str.length < 2083 && url.test(str);
}

module.exports = {
    isURL,
    customRequest
};