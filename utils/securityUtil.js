const fs = require('fs');
const crypto = require('crypto')

function log(req) {
    let dir = './security/'
    let file = 'seclog.log'

    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
    }

    if (!fs.existsSync(dir + file)) {
        fs.writeFileSync(dir + file, "");
    }

    let toSave = { 'Date': new Date(), 'url': req.url, 'type': req.method, 'headers': req.headers, 'body': req.body, 'reqIp': req.ip };
    let toSaveString = JSON.stringify(toSave)

    toSaveString += "\n"

    fs.appendFile(dir + file, toSaveString, function (err) {
        if (err) console.log(err);
    });
}

function encrypt(plaintext) {
    let cur_IV = getRandomIV();

    let encryptor = crypto.createCipheriv(getAlgorithm(), getKey(), cur_IV);
    var cipher_text = encryptor.update(plaintext, 'utf8', 'hex')
    cipher_text += encryptor.final('hex')

    let hmac = crypto.createHmac(getHmacAlgorithm(), getHmacKey());
    hmac.update(cipher_text);
    hmac.update(cur_IV.toString('hex'));

    let encrypted = cur_IV.toString('hex') + '.' + hmac.digest('hex') + '.' + cipher_text;
    return encrypted
}

function decrypt(ciphertext) {
    let cipherwords = ciphertext.split('.');
    let iv = cipherwords[0];
    let hmac_text = cipherwords[1];
    let encrypted_text = cipherwords[2];

    let hmac = crypto.createHmac(getHmacAlgorithm(), getHmacKey());
    hmac.update(encrypted_text)
    hmac.update(iv);
    let sig = hmac.digest('hex');

    if (sig != hmac_text) {
        console.log('Signature is not correct. Returning...')
        return false;
    }

    //Convert to buffer again, so decrypting can start
    iv = Buffer.from(iv, 'hex');
    // encrypted_text = Buffer.from(encrypted_text, 'hex');

    let decipher = crypto.createDecipheriv(getAlgorithm(), getKey(), iv);
    var text = decipher.update(encrypted_text, 'hex', 'utf8');
    text += decipher.final('utf8');

    return text;
}

function getKey() {
    var config = getConfig();
    let key = config.key;
    if (!key) {
        config.key = Buffer.from(crypto.randomBytes(32)).toString('hex');
        key = config.key
        saveConfig(config)
    }
    return Buffer.from(key, 'hex');
}

function getHmacKey() {
    var config = getConfig();
    let hmac_key = config.hmac_key;
    if (!hmac_key) {
        config.hmac_key = Buffer.from(crypto.randomBytes(32)).toString('hex');
        hmac_key = config.hmac_key
        saveConfig(config)
    }
    return Buffer.from(hmac_key, 'hex');
}

function getRandomIV() {
    let iv = Buffer.from(crypto.randomBytes(16));
    return iv;
}

function getAlgorithm() {
    return 'aes-256-cbc';
}

function getHmacAlgorithm() {
    return 'SHA256'
}

function getConfig() {
    let dir = './config/'
    let file = 'securityConfig.json'
    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
    }

    if (!fs.existsSync(dir + file)) {
        fs.writeFileSync(dir + file, JSON.stringify({}));
    }

    var content = fs.readFileSync(dir + file, 'utf8');
    return JSON.parse(content);
}

function saveConfig(config) {
    let dir = './config/'
    let file = 'securityConfig.json'

    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
    }

    fs.writeFileSync(dir + file, JSON.stringify(config));
}

module.exports = {
    log,
    encrypt,
    decrypt
};