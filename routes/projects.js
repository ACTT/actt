var express = require('express');
var utils = require("../utils/projectUtils");
var scanUtils = require("../utils/scanUtils");
var mail = require('../utils/mailUtils');


var router = express.Router();

/* GET project page page. */
router.get('/all', function (req, res, next) {
    var result = utils.getProjects();
    if (result) {
        res.send(result);
    } else {
        res.status(500);
        res.send('cannot get all projects');
    }
});

router.get('/:name', function (req, res, next) {

    var result = utils.getProjectByName(req.params.name); 
    if (result) {
        res.send(result);
    }
    res.status(500)
    res.send('Cannot get project by name');
});

router.get('/:name/deleteProjectByName', function (req, res, next) {

    var result = utils.deleteProjectByName(req.params.name);
    if (result) {
        res.send(result);
    }
    res.status(500);
    res.send('Cannot delete project by name');
});

router.post('/createOrUpdateProject', function (req, res, next) {
    var result = utils.createOrUpdateProject(req.body);
    if (result) {
        res.send(result);
    }
    res.status(500);
    res.send('Cannot save Project');
});

router.get('/:name/report', function (req, res, next) {

    var result = utils.generateReport(req.params.name);
    if (result) {
        res.send(result);
    }
    res.status(500);
    res.send('Cannot generate report');
});

router.post('/:name/createOrUpdateRequest', function (req, res, next) {
    var result = utils.createOrUpdateRequest(req.params.name, req.body);
    if (result) {
        res.send(result);
    }
    res.status(500);
    res.send('Cannot create or update request');
});

router.get('/:name/getRequestById/:id', function (req, res, next) {
    var result = utils.getRequestById(req.params.name, req.params.id);
    if (result) {
        res.send(result);
    }
    res.status(500);
    res.send('Cannot get request by id');
});

router.get('/:name/deleteRequest/:id', function (req, res, next) {
    var result = utils.deleteRequest(req.params.name, req.params.id);
    if (result) {
        res.send(result);
    }
    res.status(500);
    res.send('Cannot delete request');
});

router.post('/:name/createOrUpdateLoginRequest/', function (req, res, next) {
    // console.log(req.body);
    var result = utils.createOrUpdateLoginRequest(req.params.name, req.body)
    if (result) {
        res.send(result);
    }
    res.status(422);
    res.send('Invalid request parameters')
});

router.get('/:name/getLoginRequest/:userType', function (req, res, next) {
    var result = utils.getLoginRequest(req.params.name, req.params.userType);
    if (result) {
        res.send(result);
    }
    res.status(500);
    res.send('Cannot get login request');
});

router.get("/:name/deleteLoginRequest/:userType", function (req, res, next) {
    var result = utils.deleteLoginRequest(req.params.name, req.params.userType);
    if(!result.error){
        res.send(result);
    }
    res.status(422);
    res.send(result.error);
});
router.post('/:name/addHeader', function (req, res, next) {
    var result = utils.addHeader(req.params.name, req.body);
    if (result) {
        res.send(result);
    }
    res.status(500);
    res.send('Cannot add header');
});

router.post('/:name/deleteHeaderByKey', function (req, res, next) {
    var result = utils.deleteHeaderByKey(req.params.name, req.body);
    if (result) {
        res.send(result);
    }
    res.status(500);
    res.send('Cannot delete header by key');
});

// router.post('/:name/addUnauthorizedRequirements', function (req, res, next) {
//     var result = utils.addUnauthorizedRequirements(req.params.name, req.body.requesturl, req.body.codes, req.body.bodyreg, req.body.headerreg);
//     if (result) {
//         res.send(result);
//     }
//     res.status(500);
//     res.send('Cannot add unauthorized requirements');
// });

router.get('/:name/startscan', function (req, res, next) {
    let project = utils.getProjectByName(req.params.name);
    if (!project) {
        res.status(500);
        res.send('Cannot find project');
    }
    var result = scanUtils.startScan(project);
    if (result) {
        res.send(result);
    }
    res.status(500);
    res.send('An unkown error occured');
});

module.exports = router;