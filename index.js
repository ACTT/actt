var express = require('express');
var bodyParser = require('body-parser');
var projectsRouter = require('./routes/projects.js');
var projectUtils = require('./utils/projectUtils.js');
var mail = require('./utils/mailUtils')
var security = require('./utils/securityUtil')
// console.log(process.env.NODE_ENV)

mail.setServer();

var app = express();
var cors = require('cors');

app.use(cors());
app.use(express.json());

app.use(bodyParser.json({
    limit: "50mb"
}));
app.use(bodyParser.urlencoded({
    limit: "50mb",
    extended: true,
    parameterLimit: 50000
}));

app.use(function (req, res, next){
    security.log(req)
    next();
});


app.get('/', function (req, res) {
    res.send("Hello world!");
});


app.use('/projects', projectsRouter);

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;

    // render the error page
    res.status(err.status || 500);
});

projectUtils.checkDirAndCreate("./data");
projectUtils.checkDirAndCreate("./data/projects");


app.listen(3001);

// var test = require("./utils/requestUtils")
// var scan = require("./utils/scanUtils")
// var project = {};
// project.name = "theverybesttest"

// let req = {};
// req.url = "http://localhost:8080/test/login"
// let b = {}
// b.username = "admin"
// b.password = "adminpass"
// req.body = b
// req.requestMethod = "post"
// req.authType = "plaintext"
// req.requestType = "login"
// req.userType = "admin"
// project.loginRequests = []
// project.loginRequests.push(req);

// let req3 = {};
// req3.url = "http://localhost:8080/test/login"
// req3.body = {}
// req3.body.username = "normie"
// req3.body.password = "normiepass"
// req3.requestMethod = "post"
// req3.authType = "plaintext"
// req3.requestType = "login"
// req3.userType = "normie"
// project.loginRequests.push(req3);

// let req2 = {};
// req2.url = "http://localhost:8080/test/admin"
// req2.header = {}
// req2.tokenKey = "token"
// req2.requestMethod = "post"
// req2.requestType = "test"
// // req2.statusCodes = [401]
// req2.bodyRegEx = 'unauthorized'
// project.requests = []
// project.requests.push(req2);

// let req4 = {};
// req4.url = "http://localhost:8080/test/normie"
// req4.header = {}
// req4.tokenKey = "token"
// req4.requestMethod = "post"
// req4.requestType = "test"
// req4.statusCodes = [401]
// req4.bodyRegEx = 'unauthorized'
// project.requests.push(req4);

// // // projectUtils.updateProject(project)
// // projectUtils.saveProject(project)

// scan.startScan(projectUtils.getProjectByName("theverybesttest"))