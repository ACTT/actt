'use strict';
const nodemailer = require('nodemailer');
var request = require("request");
var projectUtils = require("./projectUtils.js")
var table = require('tableify');

var conf;

let transporter;
const sleep = (milliseconds) => {
    return new Promise(resolve => setTimeout(resolve, milliseconds))
  }
  

function sendMail(project) {
    //buildMailText(project.lastscan).then((toSend) => {
    // console.log(project)
    nodemailer.createTestAccount((err, account) => {
        console.log("=============GOING=TO=SLEEP=NOW!=================")
        sleep(10000).then(() => {
        var tableStyle = `border: 1px solid #000000; !important font-size: 16px; font-family:\'Calibri\',\'Verdana\',\'Arial\',\'Comic-Sans MS\'`

        // var deltaCount = (JSON.stringify(project.lastscan.delta).match(/\:false/) || []).length;
        // deltaCount += (JSON.stringify(project.lastscan.delta).match(/\:true/g) || []).length;
        // let results = JSON.parse(JSON.stringify(project.lastscan))
        // delete results.delta;

        // var resultCount = (JSON.stringify(results).match(/\:false/) || []).length;
        // resultCount += (JSON.stringify(results).match(/\:true/g) || []).length;

        // console.log('===================')
        // console.log(resultCount)
        // console.log(results)

        let date = new Date(project.lastscan.date);
        let options = { weekday: 'short', year: 'numeric', month: 'numeric', day: 'numeric' };
        let time = date.getHours() + ':' + (date.getMinutes()<10?'0':'') + date.getMinutes();

        var breakHTML = '<br>'

        var titleSend = 'There are scan results available for your project: <b>' + project.name + '</b>';

        var bodySendL1 = 'The scan results were generated on ' + date.toLocaleDateString('nl-NL', options) 
            + ' at ' + time + '.';
        var bodySendL1HTML = 'The scan results were generated on <b>' + date.toLocaleDateString('nl-NL', options)
            + '</b> at <b>' + time + '</b>.';

        // var bodySendL2 = 'The project contains ' + deltaCount + ' delta(s) and ' + resultCount + ' result(s).'
        // var bodySendL2HTML = 'The project contains <b>' + deltaCount + '</b> delta(s) and <b>' + resultCount + '</b> result(s).'

        var bodySendL2HTML = "<table>"
        let result = project.lastscan;
        let delta = result.delta;
        delete result.delta;
        delete result.date;
        
        bodySendL2HTML += "<tr><td><b>Results</b></td></tr>"
        Object.keys(result).forEach((k) => {
            let resultString = resultToString(JSON.stringify(result[k]));
            bodySendL2HTML += "<tr><td><b>" + k + ":</b></td><td>" + resultString + "</td></tr>";
        });        

        if(delta) {
            bodySendL2HTML += "<tr><td>" + breakHTML + "</td></tr>";
            bodySendL2HTML += "<tr><td><b>Delta's</b></td></tr>";
            Object.keys(delta.added).forEach((k) => {
                let resultString = resultToString(JSON.stringify(delta.added[k]));
                bodySendL2HTML += "<tr><td><b>ADDED " + k + ":</b></td><td>" + resultString + "</td></tr>";
            });
            Object.keys(delta.deleted).forEach((k) => {
                let resultString = resultToString(JSON.stringify(delta.deleted[k]));                  
                bodySendL2HTML += "<tr><td><b>DELETED " + k + ":</b></td><td>" + resultString + "</td></tr>";
            });
            Object.keys(delta.updated).forEach((k) => {
                let resultString = resultToString(JSON.stringify(delta.updated[k]));            
                bodySendL2HTML += "<tr><td><b>UPDATED " + k + ":</b></td><td>" + resultString + "</td></tr>";
            });
        }

        bodySendL2HTML += "</table>"
        // return; // NIET VERGETEN WEG TE HALEN

        var bodySendL1End = 'More detailed results can be found in your local ACTT web application.'
        var bodySendL2End = 'Please do not reply to this email, this inbox is not monitored.'

        

        var sendHTML = '<div style="color:black; font-size: 16px; font-family:\'Calibri\',\'Verdana\',\'Arial\',\'Comic-Sans MS\'">' 
            + titleSend + breakHTML + bodySendL1HTML + breakHTML + breakHTML + bodySendL2HTML + breakHTML + breakHTML 
            + bodySendL1End + breakHTML + bodySendL2End + '</div>'
        //+ '<p style="color:black; font-size: 14px; font-family:\'Calibri\',\'Verdana\',\'Arial\',\'Comic-Sans MS\'">' + bodySendL2End + '</p>'

        //console.log('====================')
        //console.log(project.lastscan)


        //bodySend = bodySend.replace(/<table>/g, '<table style="' + tableStyle + '">')
        //bodySend = bodySend.replace(/<tr/g, '<tr style="' + tableStyle + '"')
        //bodySend = bodySend.replace(/<td/g, '<td style="' + tableStyle + '"')

        //console.log(sendHTML);
        var URL = 'http://localhost:3000/projects/results/' + project.name
        //var htmlshit = '<b>Scan results for: ' + projectName + '</b></br></br><p>The scan results can be found <a href="' + URL + '">here</a></p>'
        //console.log("hiernu")
        let mailOptions = {
            from: conf.name + " <" + conf.user + ">", // sender address
            to: conf.to, // list of receivers
            subject: 'ACTT | Scan results are available for your project: ' + project.name, // Subject line
            text: titleSend + '\n\n' + bodySendL1 + '\n\n' + bodySendL1End //'Scan results for: ' + projectName +'\n\nThe scan results can be found on this URL: ' + URL, // plain text body
            , html: sendHTML //html: body // html body
        };

        // send mail with defined transport object
        transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
                return console.log(error);
            }
            console.log("Email send succesfully!")
        });
            //do stuff
        })
          
    });
    //})
}

function resultToString(result) {
    var regex1 = new RegExp('"', 'g');
    var regex2 = new RegExp(',', 'g');
    var regex3 = new RegExp(':', 'g');

    let resultString = result;

    resultString = resultString.replace('{','');
    resultString = resultString.replace(regex1, '');
    resultString = resultString.replace(regex2, ', ');
    resultString = resultString.replace(regex3, ' = ');
    resultString = resultString.replace('}','');

    return resultString;
}

function setServer() {
    if (process.env.NODE_ENV === 'GMAIL') {
        conf = require('../config/gmail.json');
        setServerGmail();
    } else if (process.env.NODE_ENV === 'SMTP') {
        conf = require('../config/smtp.json');
        setServerSMTP();
    }
    else {
        conf = require('../config/smtp.json');
        setServerSMTP();
    }
}

function setServerGmail() {
    transporter = nodemailer.createTransport({
        service: "Gmail",
        auth: {
            user: conf.user,
            pass: conf.pass
        }
    });
}

function setServerSMTP() {
    transporter = nodemailer.createTransport({
        host: conf.host,
        port: conf.port,        
        auth: {
            user: conf.user,
            pass: conf.pass
        }
    });
}

// function buildMailText(obj) {
//     return new Promise((resolve, reject) => {
//         let toSend = "Date: " + obj.date + "<br>";
//         Object.keys(obj).forEach((key) =>{
//             switch (key) {
//                 case "date":
//                     break;
//                 case "delta":
//                     //toSend += JSON.stringify(obj)
//                     toSend = toSend + "delta: \n"
//                     Object.keys(obj[key]).forEach((status) => {
//                     Object.keys(obj[key]).forEach((user) => {
//                         toSend = toSend + status + '\t'+ key + '\t' + user + '\t' +obj[key][status][user] + "<br>"
//                        })
//                     })
//                     break;
//                 default:
//                    // toSend += key + ": " + obj[key]
//                    Object.keys(obj[key]).forEach((user) => {
//                     toSend = toSend + key + '\t' + user + '\t' +obj[key][user] + "<br>"
//                    })
//                     break;
//             }
//         });
//         resolve(toSend);
//     })
//     //return JSON.stringify(project.lastscan)
// }

// function scrapeHTML(projectName){
//     return new Promise((resolve, reject) => {
//         request({
//             //uri: "http://localhost:3000/projects/results/" + projectName,
//             uri: "https://harmveraa.nl"
//             }, function(error, response, body) {
//                 if(!error){
//                     console.log(body)
//                     resolve(body)
//                 }else{
//                     console.log("joepie")
//                     reject(error)
//                 }
//             });
//     });
// }

module.exports = { sendMail, setServer };
