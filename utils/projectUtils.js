var fs = require('fs');
var path = require('path')
var requestUtils = require('./requestUtils')
var uuid = require('uuid/v1');
const security = require('./securityUtil')

function createOrUpdateProject(data) {
    var safeName = path.normalize(data.name).replace(/^(\.\.[\/\\])+/, '');
    if (!projectExists("./data/projects/" + safeName)) {
        var filePath = "./data/projects/" + safeName + "/" + safeName + ".json";

        var project = data;
        var requests = data.requests;
        // delete project["requests"];
        project.scanIsRunning = false;

        let content = JSON.stringify(project);
        var encrypted = security.encrypt(content);
        fs.writeFileSync(filePath, encrypted, 'utf8');
        return getProjectByName(project.name);

    } else {
        updateProject(data);
        return getProjectByName(data.name);
    }
}

function updateProject(project) {
    var projPath = "./data/projects/" + project.name;
    if (!projectExists(projPath)) {
        return false;
    }
    var filePath = projPath + "/" + project.name + ".json";

    let content = JSON.stringify(project);
    var encrypted = security.encrypt(content);
    fs.writeFileSync(filePath, encrypted);
    return getProjectByName(project.name);
}

function checkDirAndCreate(dir) {
    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
        return true;
    }
    return false;
}

function projectExists(dir) {
    if (checkDirAndCreate(dir)) {
        return false;
    }
    return true;
}

function getProjects() {
    var projects = [];
    var items = fs.readdirSync("./data/projects/")
    for (var i = 0; i < items.length; i++) {
        projects[i] = getProjectByName(items[i])
    }
    return projects;
}

function getProjectByName(name) {
    var filePath = "./data/projects/" + name + "/" + name + ".json";
    if (!fs.existsSync(filePath)) {
        return false;
    } else {
        var content = fs.readFileSync(filePath, 'utf8');
        var deccontent = security.decrypt(content);
        if (deccontent) {
            return JSON.parse(deccontent);
        }
        return false;
    }
}

function deleteProjectByName(name) {
    var filePath = "./data/projects/" + name + "/" + name + ".json";
    var dirPath = "./data/projects/" + name;

    if (!fs.existsSync(filePath)) {
        return false;
    }

    fs.unlinkSync(filePath);
    fs.rmdirSync(dirPath);
    return "done!";
}

function createOrUpdateRequest(name, data) {
    // if (requestUtils.isURL(data.url)) {
    //     return false;
    // }


    var project = getProjectByName(name)
    if (project === false) {
        return false;
    }
    if (!project.requests) {
        project.requests = [];
    }

    // check if id exists
    for (var i in project.requests) {
        var request = project.requests[i];
        if (request.id == data.id) {
            project.requests[i] = data;
            updateProject(project);
            return project;
        }
    }
    data.id = uuid();
    project.requests.push(data);
    updateProject(project);
    return project;
}

function getRequestById(name, id) {
    var project = getProjectByName(name);
    if (project === false) {
        return false;
    }

    for (var i in project.requests) {
        var request = project.requests[i];
        if (request.id === id) {
            return request;
        }
    }
    return false;
}

function deleteRequest(name, id) {
    var project = getProjectByName(name);
    if (project === false) {
        return false;
    }
    for (var i in project.requests) {
        var request = project.requests[i];
        if (request.id === id) {
            project.requests.splice(i, 1);
            updateProject(project);
            return project;
        }
    }
    return false;
}

function createOrUpdateLoginRequest(name, loginRequest) {
    if (loginRequest.authType !== "header" && loginRequest.authType !== "json" && loginRequest.authType !== "plaintext") {
        return false;
    }

    var project = getProjectByName(name);
    if (!project) {
        return false;
    }

    for (var res in project.loginRequests) {
        if (project.loginRequests[res].userType === loginRequest.userType) {
            project.loginRequests[res] = loginRequest;
            updateProject(project);
            return project;
        }
    }

    if (!project.loginRequests) {
        project.loginRequests = [];
    }

    project.loginRequests.push(loginRequest);
    updateProject(project);
    return project;
}

function getLoginRequest(name, userType) {
    var project = getProjectByName(name);
    if (project === false) {
        return false;
    }

    for (var i in project.loginRequests) {
        var loginRequest = project.loginRequests[i];
        if (loginRequest.userType === userType) {
            return loginRequest;
        }
    }
    return false;
}

function deleteLoginRequest(projectName, userType) {

    var result = {};

    var project = getProjectByName(projectName);

    if (!project) {
        result.error = 'No such project.';
        return result;
    }

    if (!project.loginRequests) {
        result.error = 'No such user.';
        return result;
    }

    for (var i in project.loginRequests) {
        var loginRequest = project.loginRequests[i];
        if (loginRequest.userType === userType) {
            project.loginRequests.splice(i, 1);
            updateProject(project);
            return project;
        }
    }
    result.error = 'No such user.';
    return result;

    // var todel = [];
    // for (var res in project.loginRequests) {
    //     if (project.loginRequests[res] && project.loginRequests[res].userType === userType) {
    //         todel.push(res);
    //     }
    // }

    // if (todel.length === 0) {
    //     result.error = 'No such user.';
    //     return result;
    // }

    // for (var del in todel) {
    //     delete project.loginRequests[del];
    // }

    // project.loginRequests = project.loginRequests.filter(function (el) {
    //     return el != null;
    // });

    // updateProject(project);
    // result.result = getProjectByName(projectName)
    // return result;
}

function addHeader(name, data) {
    var project = getProjectByName(name)
    if (project === false) {
        return false;
    }

    if (!project.headers) {
        project.headers = {};
    }

    Object.assign(project.headers, data);

    updateProject(project);
    return project;
}

function deleteHeaderByKey(name, data) {
    var project = getProjectByName(name)
    if (project === false) {
        return false;
    }

    delete project.headers[data.key];

    updateProject(project);
    return project;
}

// function addUnauthorizedRequirements(projectName, requesturl, codes, bodyreg, headerreg) { //TODO
//     var project = getProjectByName(projectName)

//     if (project === false)
//     {
//         return false;
//     }

//     var updated = false;
//     for (var i in project.requests) {
//         var request = project.requests[i];
//         if (request.url == requesturl) {
//             request.bodyRegEx = bodyreg;
//             request.HeaderRegEx = headerreg
//             request.statusCodes = codes;
//             updated = true;
//         }
//     }

//     if(updated){
//         updateProject(project);
//         return project;
//     }
//     return false;
// }

function generateReport(name) { //TODO

}

module.exports = {
    getProjects,
    getProjectByName,
    deleteProjectByName,
    createOrUpdateProject,
    checkDirAndCreate, // voor testen
    createOrUpdateRequest,
    getRequestById,
    deleteRequest,
    createOrUpdateLoginRequest,
    getLoginRequest,
    deleteLoginRequest,
    addHeader,
    deleteHeaderByKey,
    updateProject,
    // addUnauthorizedRequirements,
    generateReport
};