var requestUtils = require("./requestUtils.js");
var projectUtils = require("./projectUtils.js");
const { detailedDiff, updatedDiff } = require("deep-object-diff");
var mail = require("./mailUtils")

function startScan(project) {
    if (!project.requests.length > 0 || !project.loginRequests.length > 0 || project.scanIsRunning) {
        return false;
    }

    var authCreds;
    getAuth(project).then(creds => {
        authCreds = creds;
        let currentRequest = 0;
        let maxRequests = Object.keys(authCreds).length * project.requests.length;
        console.log("Starting scan");
        var now = new Date();
        project.lastAttemptedScanDate = now;
        project.scanIsRunning = true;
        projectUtils.updateProject(project);
        var thisScanObject = { date: now };
        new Promise((resolve, reject) => {
            project.requests.forEach(req => {
                new Promise((resolve, reject) => {
                    for (var userType in authCreds) {
                        if (!req.headers) req.headers = {};
                        req.headers[req.tokenKey] = authCreds[userType];
                        requestUtils.customRequest(req, userType).then(result => {
                            currentRequest += 1;
                            if (!thisScanObject[req.url]) {
                                thisScanObject[req.url] = {};
                            }
                            thisScanObject[req.url][result.user] = result.allowed;
                            if (maxRequests === currentRequest) {
                                if (project.results === undefined) {
                                    project.results = [];
                                }
                                project.results.push(thisScanObject);
                                project.lastscan = thisScanObject;
                                resolve(project);
                            }
                        }, err => (reject(err)));
                    }
                }).then(project => {
                    resolve(project);
                }, err => { reject(err) });
            });
        })
            .then(project => {
                console.log("Stopping scan and saving project");
                // Handle Delta? Move the update to handle delta aswell? Probably another promise yo
                project.requests.forEach((req) => {
                    delete req.headers[req.tokenKey]
                });
                
                handleScanDelta(project);

                // projectUtils.updateProject(project);
            }, err => { 
                project.scanIsRunning = false;
                projectUtils.updateProject(project);
                console.log("scan failed " + project.name + " because of: \n" + JSON.stringify(err));
            })
            .catch(err => {
                console.log(err);
            });
    });
    return "Scan started";
}

function getAuth(project) {
    return new Promise(resolve => {
        var authCreds = {};
        var last = project.loginRequests[project.loginRequests.length - 1];
        project.loginRequests.forEach(req => {
            requestUtils
                .customRequest(req)
                .then(result => {
                    authCreds[req.userType] = result;
                    if (req.userType === last.userType) {
                        resolve(authCreds);
                    }
                })
                .catch(err => {
                    console.log(err);
                });
        });
    });
}

async function handleScanDelta(project) {
    // Compare results[length -1] vs results[length -2] if they differ they go into delta object
    if (project.results.length >= 2) {
        let delta = await createDelta(
            project.results[project.results.length - 1],
            project.results[project.results.length - 2],
            project.goal,
        );
        if (delta) project.results[project.results.length - 1].delta = delta;        
    }
    project.scanIsRunning = false;
    projectUtils.updateProject(project);
    mail.sendMail(project);    
}

async function createDelta(curScan, prevScan, goal) {
    let prev = JSON.parse(JSON.stringify(prevScan));
    delete prev.delta;
    let delta = detailedDiff(prev, curScan);2
    if (goal) {
        let goalDelta = updatedDiff(goal, curScan);
        delta.updated = { ...delta.updated, ...goalDelta };
    }
    

    Object.keys(delta.deleted).forEach(function (key) {
        delta.deleted[key] = prev[key]
    })
    delete delta.updated.date;
    delete delta.deleted.date;
    delete delta.added.date;
    console.log(delta)
    return delta;
}

module.exports = {
    startScan,
};
